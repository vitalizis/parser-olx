<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use ParserController;

use App\ParserResult;

use Auth;

class MainController extends Controller
{
	public function parse(){
		$parse = new ParserController();
		$parse->parser($url);
	}
//===========================================
    public function index()
    {
        return ParserResult::latest()->get();
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'link' => 'required'
        ]);
		$fields = app('App\Http\Controllers\ParserController')->parser($request->link);
        return ParserResult::create([ 'link' => request('link'),'name'=>$fields['name'],'phone'=>$fields['phone'],'user_id'=>\Auth::user()->id]);
    }

}
