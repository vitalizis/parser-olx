<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParserResult extends Model
{
   protected $guarded = [];
}
